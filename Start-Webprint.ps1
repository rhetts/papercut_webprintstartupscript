<#
.Synopsis
Webprint Startup
.DESCRIPTION
The webprint servers need to have a workstation logged in, in order to
properly operate.  This script will check to see if the webprint service
account logged in, then lock the screen, next it will check to see if the
connection to the server's file share is mapped, and finaly start the
PaperCut webprint service.
.EXAMPLE
.\Start-Webprint.ps1
.INPUTS

.OUTPUTS
Log file is created/amended on the webprint server.
.FUNCTIONALITY
Runs as a logon script.
#>

####################### Variables #######################

$LogPath = "D:\Scripts\Logs\"
$LogName = "WebbprintStartup.log"
$LogFile = $LogPath + $LogName
$BlankLine = " "
$TodaysDate = (Get-Date).ToLongDateString()

$LocalUser = $Null
$IWpStatus = $Null
$Before = "0"
$MBefore = "0"
$After = "0"
$VerifyPc = "0"

$ServerName = $env:computername
$LServerName = $ServerName.ToLower() #Convert server name to lower case
$PathStatus = "W:\$LServerName.web-print-server.status"    


####################### Functions #######################

function Write-Log {
    Param(
        $Message
    )
    function TS {Get-Date -Format 'HH:mm:ss'}
    "[$(TS)]     $Message" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
}


Function Get-LogStatus {
    # Checks to see if a log file exists and if not creates one
    If (Test-Path $LogFile) {
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        "               $TodaysDate" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
    }
    Else {
        New-Item $LogFile -Force -ItemType File
        "               $TodaysDate" | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
        $BlankLine | Tee-Object -FilePath $LogFile -Append | Write-Verbose
    }
}


Function Check-BeforePcStatus {
    Get-Process pc-web-print | % { $_.ID}
}
$Before = Check-BeforePcStatus


Function Check-AfterPcStatus {
    $After = Get-Process pc-web-print | % { $_.Id } | ? { $before -notcontains $_ }
}
$After = Check-AfterPcStatus



Function Get-UserLoggedIn {
    Write-Log "Detected new user logged in.  Checking to see who just logged in to this workstation."
    $LocalUser = $env:username
    # Checks to see if the user is the psu\webprint user.  If not, the script exists.
    If ($LocalUser -ne "webprint") {
        #Write-EventLog -EventId 50001 -LogName WST -Message "The PSU\Webprint user did not logon, logon script will not run." -Source User32 -EntryType Information
        Write-Log "     The user PSU\$LocalUser logged in.  This script should only run when the PSU\webprint user logs in.  Existing logon script."
        Write-Log "----- Webprint Startup Script Completed -----"
        Exit
    }
    Else {
        # This section section below will automatically lock the desktop at login.
        #Write-EventLog -EventId 50000 -LogName WST -Message "The PSU\Webprint user logged in." -Source User32 -EntryType Information
        Write-Log "     The PSU\Webprint user logged in, continuing with logon script."
    }
}


Function Check-InitialWebprintStatus {
    $Before
    Write-Log "Initial Check to see if the Webprint application is running"
    $MBefore = $Before.Count
    If ($MBefore -eq 0) {
        Write-Log "     The Webprint application is not running, continuing to process script.";
        Return;
    }
    If ($MBefore -eq 1) {
        Write-Log "Webprint application is already running, exiting script.";
        Write-Log "----- Webprint Startup Script Completed -----";
        Exit
    }
    If ($MBefore -gt 1) {
        Write-Log "     There are multiple instances of the Webprint application already running.  Killing all processes.";
        Get-Process pc-web-print | Stop-Process;
        Write-Log "     Continuing to process script.";
        Return;
    }
}


Function Initialize-ScreenLock {
    $WinLock = $Null
    $ScreenStatus = "0"
    $Error.Clear()
    $yesterday = (Get-Date) - (New-TimeSpan -Minutes 5)
    Do {
        Try {
            $WinLock = Get-WinEvent -FilterHashTable @{LogName='Security'; ID=4800; StartTime=$yesterday} -MaxEvents 1 -ErrorAction Stop
            [string]$ScreenStatus = $WinLock.Id
        }
        Catch {
            Write-Log "The initial check of the screen shows it is not locked.";
            If ($ScreenStatus -ne 4800) {
                Write-Log "     Locking The Screen."
                Invoke-Command -ScriptBlock {cmd /c "C:\Windows\System32\rundll32.exe user32.dll,LockWorkStation"}
                Start-Sleep -Seconds 5
            }
        }
    }
    Until ($ScreenStatus -eq 4800)
    Write-Log "     The console screen has been locked."
}


Function Initilize-HotFolder {
    $hf = 1
    Write-Log "Performing initial test to see if the W: Drive is mapped."
    If (Test-Path -Path $PathStatus) {
       Write-Log "     W: Drive is mapped."
       Return;
    }
    Else {
        Do {
            If (Test-Path -Path $PathStatus) {
                Return;
            }
            Else {
                Write-Log "     Attempting $hf times to map the W: Drive.";
                (New-Object -ComObject WScript.Network).MapNetworkDrive('W:','\\labspcclient.print.pdx.edu\PCWebprint');
                $hf++;
                Start-Sleep -Seconds 5;
                (Test-Path -Path $PathStatus);
            }
        }
        Until (Test-Path -Path $PathStatus)
    }
    Write-Log "     W: Drive is mapped."
}


Function Initilize-Webprint {
#    $PCApp = "D:\PaperCutNg\providers\web-print\win\pc-web-print.exe"
    $MBefore = $Before.Count
    Do {
        If ($MBefore -eq 0) {
            Write-Log "Starting the Webprint application.";
            & D:\PaperCutNg\providers\web-print\win\pc-web-print.exe;
            Write-Log "     Webprint application has started.";
            Return;
        }
        Else {
            If ($MBefore -ne 0) {
                Write-Log "Webprint application is already running, exiting script.";
                Write-Log "----- Webprint Startup Script Completed -----";
                Exit
            }
        }
        $After
        If ($After -ne "0") {
            Write-Log "     The Webprint application has already been initilized, exiting script.";
            Return;
        }
        Else {
            Write-Log "     The Webprint application failed to start.  Trying again in 5 seconds.";
            Start-Sleep -Seconds 5;
            Return;
        }
    }
    Until ($After -ne "0")
}


Function Verify-Webprint {
    $VerifyPc = Get-Process pc-web-print | % { $_.ID};
    $PcProcess = Get-Process -ID $VerifyPc;
    $MVerifyPc = $VerifyPc.Count;
    If ($MVerifyPc -eq 1) {
        $PcProcess.PriorityClass = 'AboveNormal';
        Write-Log "Verified the Webprint application has started.";
        Write-Log "     PC-Web-Print Process ID is $VerifyPc.";
        $VerifyPcProcess = $PcProcess.PriorityClass
        Write-Log "     PC-Web-Print Process Priority has been set to $VerifyPcProcess.";
    }
}


#######################  Script Commands #######################

Get-LogStatus
Write-Log "----- Starting Webprint Application -----"
Get-UserLoggedIn
Check-InitialWebprintStatus
Initialize-ScreenLock
Initilize-HotFolder
Initilize-Webprint
Verify-Webprint
Write-Log "The Webprint application started successfully."
Write-Log "----- Webprint Startup Script Completed -----"